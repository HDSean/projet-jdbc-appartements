-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le :  jeu. 17 déc. 2020 à 15:33
-- Version du serveur :  5.7.17
-- Version de PHP :  5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `tpnote`
--

-- --------------------------------------------------------

--
-- Structure de la table `appartement`
--

CREATE TABLE `appartement` (
  `idAppartement` int(11) NOT NULL COMMENT 'id de l''appartement',
  `adresse` varchar(50) NOT NULL COMMENT 'adresse de l''apt',
  `description` varchar(200) NOT NULL COMMENT 'description de l''apt',
  `enConstruction` tinyint(1) NOT NULL COMMENT 'etat de construction de l''apt'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `appartement`
--

INSERT INTO `appartement` (`idAppartement`, `adresse`, `description`, `enConstruction`) VALUES
(1, '7 allée Guy Charff, res technopole...', 'une description random ...', 0),
(2, '8 allée Guy Charff, res technopole...', 'une deuxieme description', 1),
(3, '9 allee guy charff', 'descr 9', 0);

-- --------------------------------------------------------

--
-- Structure de la table `chambre`
--

CREATE TABLE `chambre` (
  `idChambre` int(11) NOT NULL COMMENT 'id de la chambre',
  `idAppartement` int(11) NOT NULL COMMENT 'id de l''appartement',
  `typeChambre` varchar(20) NOT NULL COMMENT 'type de la chambre'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `chambre`
--

INSERT INTO `chambre` (`idChambre`, `idAppartement`, `typeChambre`) VALUES
(3, 1, 'double'),
(7, 2, 'simple'),
(8, 2, 'simple'),
(10, 3, 'simple');

-- --------------------------------------------------------

--
-- Structure de la table `cuisine`
--

CREATE TABLE `cuisine` (
  `idCuisine` int(11) NOT NULL COMMENT 'id de la cuisine',
  `idAppartement` int(11) NOT NULL COMMENT 'id de l''appartement',
  `nbPointsGaz` int(11) NOT NULL COMMENT 'nombre de points gaz'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `cuisine`
--

INSERT INTO `cuisine` (`idCuisine`, `idAppartement`, `nbPointsGaz`) VALUES
(1, 1, 3),
(4, 2, 4),
(9, 3, 1);

-- --------------------------------------------------------

--
-- Structure de la table `liaison`
--

CREATE TABLE `liaison` (
  `idLiaison` int(11) NOT NULL COMMENT 'id de la liaison',
  `idAppartement` int(11) NOT NULL COMMENT 'id de l''appartement',
  `idLocal1` int(11) NOT NULL COMMENT 'id local 1',
  `idLocal2` int(11) NOT NULL COMMENT 'id local 2'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `liaison`
--

INSERT INTO `liaison` (`idLiaison`, `idAppartement`, `idLocal1`, `idLocal2`) VALUES
(1, 1, 1, 2),
(2, 1, 1, 3),
(3, 1, 2, 3),
(4, 2, 4, 5),
(5, 2, 4, 6),
(6, 2, 4, 7),
(7, 2, 4, 8),
(8, 2, 5, 6),
(9, 2, 5, 7),
(10, 2, 5, 8),
(11, 2, 6, 7),
(12, 2, 6, 8),
(13, 2, 7, 8),
(14, 3, 9, 10),
(15, 3, 9, 11),
(16, 3, 10, 11),
(17, 2, 5, 6),
(18, 3, 12, 11);

-- --------------------------------------------------------

--
-- Structure de la table `local`
--

CREATE TABLE `local` (
  `idLocal` int(11) NOT NULL COMMENT 'id du local',
  `idAppartement` int(11) NOT NULL COMMENT 'id de l''appartement',
  `surface` double NOT NULL COMMENT 'surface du local',
  `description` varchar(200) NOT NULL COMMENT 'description du local'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `local`
--

INSERT INTO `local` (`idLocal`, `idAppartement`, `surface`, `description`) VALUES
(1, 1, 8, 'cuisine'),
(2, 1, 10, 'salle de bain'),
(3, 1, 10, 'chambre '),
(4, 2, 4, 'cuisine'),
(5, 2, 5, 'salle de bain 1'),
(6, 2, 5, 'salle de bain 2'),
(7, 2, 9, 'chambre 1'),
(8, 2, 9, 'chambre 2'),
(9, 3, 1.25, 'descr cui 1 '),
(10, 3, 1.25, 'descr ch 1'),
(11, 3, 1.25, 'descr sdb 1'),
(12, 3, 5.25, 'descr sdb 2');

-- --------------------------------------------------------

--
-- Structure de la table `salledebain`
--

CREATE TABLE `salledebain` (
  `idSalleDeBain` int(11) NOT NULL COMMENT 'id de la salle de bain',
  `idAppartement` int(11) NOT NULL COMMENT 'id de l''appartement',
  `nbPointsEau` int(11) NOT NULL COMMENT 'nombre de points eau'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `salledebain`
--

INSERT INTO `salledebain` (`idSalleDeBain`, `idAppartement`, `nbPointsEau`) VALUES
(2, 1, 4),
(5, 2, 3),
(6, 2, 3),
(11, 3, 2),
(12, 3, 2);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `appartement`
--
ALTER TABLE `appartement`
  ADD PRIMARY KEY (`idAppartement`);

--
-- Index pour la table `chambre`
--
ALTER TABLE `chambre`
  ADD PRIMARY KEY (`idChambre`),
  ADD KEY `chambreidapt_appartement` (`idAppartement`);

--
-- Index pour la table `cuisine`
--
ALTER TABLE `cuisine`
  ADD PRIMARY KEY (`idCuisine`),
  ADD KEY `cuisineidapt_apt` (`idAppartement`);

--
-- Index pour la table `liaison`
--
ALTER TABLE `liaison`
  ADD PRIMARY KEY (`idLiaison`),
  ADD KEY `liaison1` (`idAppartement`),
  ADD KEY `liaison2` (`idLocal1`),
  ADD KEY `liaison3` (`idLocal2`);

--
-- Index pour la table `local`
--
ALTER TABLE `local`
  ADD PRIMARY KEY (`idLocal`),
  ADD KEY `localidappartement_appartement` (`idAppartement`);

--
-- Index pour la table `salledebain`
--
ALTER TABLE `salledebain`
  ADD PRIMARY KEY (`idSalleDeBain`),
  ADD KEY `salledebainidapt_appartement` (`idAppartement`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `appartement`
--
ALTER TABLE `appartement`
  MODIFY `idAppartement` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de l''appartement', AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT pour la table `liaison`
--
ALTER TABLE `liaison`
  MODIFY `idLiaison` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id de la liaison', AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT pour la table `local`
--
ALTER TABLE `local`
  MODIFY `idLocal` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id du local', AUTO_INCREMENT=13;
--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `chambre`
--
ALTER TABLE `chambre`
  ADD CONSTRAINT `chambreidapt_appartement` FOREIGN KEY (`idAppartement`) REFERENCES `appartement` (`idAppartement`);

--
-- Contraintes pour la table `cuisine`
--
ALTER TABLE `cuisine`
  ADD CONSTRAINT `cuisineidapt_apt` FOREIGN KEY (`idAppartement`) REFERENCES `appartement` (`idAppartement`);

--
-- Contraintes pour la table `liaison`
--
ALTER TABLE `liaison`
  ADD CONSTRAINT `liaison1` FOREIGN KEY (`idAppartement`) REFERENCES `appartement` (`idAppartement`),
  ADD CONSTRAINT `liaison2` FOREIGN KEY (`idLocal1`) REFERENCES `local` (`idLocal`),
  ADD CONSTRAINT `liaison3` FOREIGN KEY (`idLocal2`) REFERENCES `local` (`idLocal`);

--
-- Contraintes pour la table `local`
--
ALTER TABLE `local`
  ADD CONSTRAINT `localidappartement_appartement` FOREIGN KEY (`idAppartement`) REFERENCES `appartement` (`idAppartement`);

--
-- Contraintes pour la table `salledebain`
--
ALTER TABLE `salledebain`
  ADD CONSTRAINT `salledebainidapt_appartement` FOREIGN KEY (`idAppartement`) REFERENCES `appartement` (`idAppartement`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
