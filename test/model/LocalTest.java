package model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LocalTest {

	@Test
	public void constructorTestNotNullValuesAndGettersTest() {
		/*expected values*/
		int idLocal = 1;
		int idAppartement = 1;
		double surface = 11.1115;
		String description = "description local 1";
		/*actual object*/
		Local local = new Local(1, 1, 11.1115, "description local 1");
		/*vérifier que l'objet local ne soit pas null*/
		assertNotNull(local);
		/*test des getters*/
		assertEquals(idLocal,local.getIdLocal());
		assertEquals(idAppartement,local.getIdAppartement());
		assertEquals(surface,local.getSurface());
		assertEquals(description,local.getDescription());
	}
	
	@Test
	public void settersTest() {
		/*expected*/
		int idLocal = 2;
		int idAppartement = 2;
		double surface = 11.11155;
		String description = "description local 2";
		/*actual*/
		Local local = new Local(1, 1, 11.1115, "description local 1");
		local.setIdAppartement(idAppartement);
		local.setSurface(surface);
		local.setDescription(description);
		
		assertNotEquals(idLocal, local.getIdLocal());
		assertEquals(idAppartement, local.getIdAppartement());
		assertEquals(surface, local.getSurface());
		assertEquals(description, local.getDescription());
	}

}
