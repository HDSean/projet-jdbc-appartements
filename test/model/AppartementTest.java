package model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class AppartementTest {

	@Test
	public void constructorTestNotNullValuesAndGettersTest() {
		/*expected values*/
		int id = 1;
		String adresse = "l'adresse";
		String description = "la description";
		boolean enConstruction = false;
		/*actual object*/
		Appartement appartement = new Appartement(1, "l'adresse","la description", false);
		/*vérifier que l'objet appartement ne soit pas null*/
		assertNotNull(appartement);
		/*test des getters*/
		assertEquals(id,appartement.getIdAppartement());
		assertEquals(adresse,appartement.getAdresse());
		assertEquals(description,appartement.getDescription());
		assertEquals(enConstruction,appartement.isEnConstruction());
	}
	
	@Test
	public void settersTest() {
		/*expected*/
		int id = 2;
		String adresse = "adresse2";
		String description = "description2";
		boolean enConstruction = true;
		/*actual*/
		Appartement appartement = new Appartement(1,"adresse","description",false);
		appartement.setAdresse(adresse);
		appartement.setDescription(description);
		appartement.setEnConstruction(enConstruction);
		
		assertNotEquals(id, appartement.getIdAppartement());
		assertEquals(adresse, appartement.getAdresse());
		assertEquals(description, appartement.getDescription());
		assertEquals(enConstruction, appartement.isEnConstruction());
	}

}
