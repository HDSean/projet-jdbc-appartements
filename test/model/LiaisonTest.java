package model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class LiaisonTest {

	@Test
	public void constructorTestNotNullValuesAndGettersTest() {
		/*expected values*/
		int idLiaison = 1;
		int idAppartement = 1;
		int idLocal1 = 1;
		int idLocal2 = 2;
		/*actual object*/
		Liaison liaison = new Liaison(1, 1, 1, 2);
		/*vérifier que l'objet liaison ne soit pas null*/
		assertNotNull(liaison);
		/*test des getters*/
		assertEquals(idLiaison,liaison.getIdLiaison());
		assertEquals(idAppartement,liaison.getIdAppartement());
		assertEquals(idLocal1,liaison.getIdLocal1());
		assertEquals(idLocal2,liaison.getIdLocal2());
	}
	
	@Test
	public void settersTest() {
		/*expected*/
		int idLiaison = 2;
		int idAppartement = 2;
		int idLocal1 = 2;
		int idLocal2 = 3;
		/*actual*/
		Liaison liaison = new Liaison(1, 1, 1, 2);
		liaison.setIdAppartement(idAppartement);
		liaison.setIdLocal1(idLocal1);
		liaison.setIdLocal2(idLocal2);
		
		assertNotEquals(idLiaison, liaison.getIdLiaison());
		assertEquals(idAppartement, liaison.getIdAppartement());
		assertEquals(idLocal1, liaison.getIdLocal1());
		assertEquals(idLocal2, liaison.getIdLocal2());
	}
}
