package model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class CuisineTest {

	@Test
	public void constructorTestNotNullValuesAndGettersTest() {
		/*expected values*/
		int idCuisine = 1;
		int nbPointsGaz = 1;
		int idLocal = 1;
		int idAppartement = 1;
		double surface = 4.2555;
		String description = "description de la cuisine 1";
		/*actual object*/
		Cuisine cuisine = new Cuisine(1,1, 1, 1, 4.2555, "description de la cuisine 1");
		/*vérifier que l'objet cuisine ne soit pas null*/
		assertNotNull(cuisine);
		/*test des getters*/
		assertEquals(idCuisine, cuisine.getIdCuisine());
		assertEquals(nbPointsGaz, cuisine.getNbPointsGaz());
		assertEquals(idLocal, cuisine.getIdLocal());
		assertEquals(idAppartement, cuisine.getIdAppartement());
		assertEquals(surface, cuisine.getSurface());
		assertEquals(description, cuisine.getDescription());
	}
	
	@Test
	public void settersTest() {
		/*expected values*/
		int idCuisine = 1;
		int nbPointsGaz = 1;
		int idLocal = 1;
		int idAppartement = 1;
		double surface = 4.2555;
		String description = "description de la cuisine 1";
		/*actual*/
		Cuisine cuisine = new Cuisine(2, 2, 2, 2, 4.25555, "description de la cuisine 2");
		cuisine.setIdCuisine(idCuisine);
		cuisine.setNbPointsGaz(nbPointsGaz);
		cuisine.setIdLocal(idLocal);
		cuisine.setIdAppartement(idAppartement);
		cuisine.setSurface(surface);
		cuisine.setDescription(description);
		
		assertEquals(idCuisine, cuisine.getIdCuisine());
		assertEquals(nbPointsGaz, cuisine.getNbPointsGaz());
		assertEquals(idLocal, cuisine.getIdLocal());
		assertEquals(idAppartement, cuisine.getIdAppartement());
		assertEquals(surface, cuisine.getSurface());
		assertEquals(description, cuisine.getDescription());
	}

}
