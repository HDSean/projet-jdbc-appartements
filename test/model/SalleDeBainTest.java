package model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class SalleDeBainTest {
	@Test
	public void constructorTestNotNullValuesAndGettersTest() {
		/*expected values*/
		int idSalleDeBain = 1;
		int nbPointsEau = 1;
		int idLocal = 1;
		int idAppartement = 1;
		double surface = 4.2555;
		String description = "description de la sdb 1";
		/*actual object*/
		SalleDeBain salledebain = new SalleDeBain(1,1, 1, 1, 4.2555, "description de la sdb 1");
		/*vérifier que l'objet cuisine ne soit pas null*/
		assertNotNull(salledebain);
		/*test des getters*/
		assertEquals(idSalleDeBain, salledebain.getIdSalleDeBain());
		assertEquals(nbPointsEau, salledebain.getNbPointsEau());
		assertEquals(idLocal, salledebain.getIdLocal());
		assertEquals(idAppartement, salledebain.getIdAppartement());
		assertEquals(surface, salledebain.getSurface());
		assertEquals(description, salledebain.getDescription());
	}
	
	@Test
	public void settersTest() {
		/*expected values*/
		int idSalleDeBain = 1;
		int nbPointsEau = 1;
		int idLocal = 1;
		int idAppartement = 1;
		double surface = 4.2555;
		String description = "description de la sdb 1";
		/*actual*/
		SalleDeBain salledebain = new SalleDeBain(2, 2, 2, 2, 4.25555, "description de la sdb 2");
		salledebain.setIdSalleDeBain(idSalleDeBain);
		salledebain.setNbPointsEau(nbPointsEau);
		salledebain.setIdLocal(idLocal);
		salledebain.setIdAppartement(idAppartement);
		salledebain.setSurface(surface);
		salledebain.setDescription(description);
		
		assertEquals(idSalleDeBain, salledebain.getIdSalleDeBain());
		assertEquals(nbPointsEau, salledebain.getNbPointsEau());
		assertEquals(idLocal, salledebain.getIdLocal());
		assertEquals(idAppartement, salledebain.getIdAppartement());
		assertEquals(surface, salledebain.getSurface());
		assertEquals(description, salledebain.getDescription());
	}
}
