package model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class ChambreTest {

	@Test
	public void constructorTestNotNullValuesAndGettersTest() {
		/*expected values*/
		int idChambre = 1;
		String typeChambre = "simple";
		int idLocal = 1;
		int idAppartement = 1;
		double surface = 4.2555;
		String description = "description de l'appartement 1";
		/*actual object*/
		Chambre chambre = new Chambre(1,"simple", 1, 1, 4.2555, "description de l'appartement 1");
		/*vérifier que l'objet chambre ne soit pas null*/
		assertNotNull(chambre);
		/*test des getters*/
		assertEquals(idChambre, chambre.getIdChambre());
		assertEquals(typeChambre, chambre.getTypeChambre());
		assertEquals(idLocal, chambre.getIdLocal());
		assertEquals(idAppartement, chambre.getIdAppartement());
		assertEquals(surface, chambre.getSurface());
		assertEquals(description, chambre.getDescription());
	}
	
	@Test
	public void settersTest() {
		/*expected values*/
		int idChambre = 1;
		String typeChambre = "simple";
		int idLocal = 1;
		int idAppartement = 1;
		double surface = 4.2555;
		String description = "description de l'appartment 1";
		/*actual*/
		Chambre chambre = new Chambre(2,"double", 2, 2, 4.25555, "description de l'appartement 2");
		chambre.setIdChambre(idChambre);
		chambre.setTypeChambre(typeChambre);
		chambre.setIdLocal(idLocal);
		chambre.setIdAppartement(idAppartement);
		chambre.setSurface(surface);
		chambre.setDescription(description);
		
		assertEquals(idChambre, chambre.getIdChambre());
		assertEquals(typeChambre, chambre.getTypeChambre());
		assertEquals(idLocal, chambre.getIdLocal());
		assertEquals(idAppartement, chambre.getIdAppartement());
		assertEquals(surface, chambre.getSurface());
		assertEquals(description, chambre.getDescription());
	}

}
