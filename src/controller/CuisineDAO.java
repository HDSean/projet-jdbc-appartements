package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Cuisine;

/**
 * @file CuisineDAO.java
 * @author Anica Sean & Ung Alexandre
 * @version 3.0
 * @date 17 Decembre 2020
 * @brief Définit la classe CuisineDAO qui permet d'intéragir avec la base de données et de remplir les attributs
 * 		  des objets Cuisine et local en conséquence.
 * @details Cette classe hérite de DAO<Cuisine>
 */
public class CuisineDAO extends DAO<Cuisine> {

	/**
	 * 
	 * @param conn : Connection
	 */
	public CuisineDAO(Connection conn) {
		super(conn);
	}
	
	/**
	 * @brief permet de retrouver une cuisine dans la base de données.
	 * @param id : Integer
	 * @return cuisine : Cuisine
	 */
	@Override
	public Cuisine find(int id) {
		Cuisine cuisine=null;
		final String SQL_QUERY = "SELECT * FROM cuisine WHERE idCuisine=?";
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_QUERY);
			stm.setInt(1,id);
			ResultSet rs = stm.executeQuery();
			if(rs.first()) {
				cuisine = new Cuisine(id,rs.getInt("nbPointsGaz"),id,rs.getInt("idAppartement"), rs.getDouble("surface"), rs.getString("description"));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return cuisine;
	}

	/**
	 * @brief permet d'insérer une cuisine dans la base de données.
	 * @details insère aussi un local dans la table local en conséquence.
	 * @param obj : Cuisine
	 * @return etat : boolean
	 */
	@Override
	public boolean insert(Cuisine obj) {
		boolean etat = false;
		boolean etat1 = false;
		boolean etat2 = false;
		final String SQL_INSERT_LOCAL = " INSERT INTO local VALUES (NULL, ?, ?, ?)";
		/*on complète la table local en ajoutant les réréfences de la cuisine*/
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_INSERT_LOCAL);
			stm.setInt(1, obj.getIdAppartement());
			stm.setDouble(2, obj.getSurface());
			stm.setString(3, obj.getDescription());
			int result = stm.executeUpdate();
			if(result > 0) {
				etat1 = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		obj.setIdLocal(getLastIdLocal());
		/*on complète la table cuisine*/
		final String SQL_INSERT = "INSERT INTO cuisine VALUES(?, ?, ?)";
		try {
			PreparedStatement stm2 = connect.prepareStatement(SQL_INSERT);
			stm2.setInt(1, obj.getIdLocal());
			stm2.setInt(3, obj.getNbPointsGaz());
			stm2.setInt(2, obj.getIdAppartement());
			int result = stm2.executeUpdate();
			if(result > 0) {
				etat2 = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(etat1 && etat2) {
			etat = true;
		}
		return etat;
	}

	/**
	 * @brief permet de supprimer une cuisine de la base de données.
	 * @details supprimer aussi un local dans la table local en conséquence.
	 * @param obj : Cuisine
	 * @return state0 : boolean
	 */
	@Override
	public boolean update(Cuisine obj) {
		final String SQL_UPDATE = "UPDATE cuisine SET idAppartement=?, nbPointsGaz=?)";
		boolean state = false;
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_UPDATE);
			stm.setInt(1, obj.getIdAppartement());
			stm.setInt(2, obj.getNbPointsGaz());
			
			int result = stm.executeUpdate();
			if(result > 0) {
				state = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		boolean state2 = false;
		final String SQL_UPDATE_LOCAL = "UPDATE local SET idAppartement=?, surface=?, description=? WHERE idLocal=?";
		try {
			PreparedStatement stm2 = connect.prepareStatement(SQL_UPDATE_LOCAL);
			stm2.setInt(1, obj.getIdAppartement());
			stm2.setDouble(2, obj.getSurface());
			stm2.setString(3, obj.getDescription()); 
			stm2.setInt(4, obj.getIdCuisine());//idCuisine et idLocal sont les mêmes
			int result = stm2.executeUpdate();
			if(result > 0) {
				state2 = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		boolean state0 = false;
		if(state && state2) {
			state0 = true;
		}
		return state0;
	}
	
	/**
	 * @brief permet de supprimer une cuisine de la base de données.
	 * @details supprimer aussi un local dans la table local en conséquence.
	 * @param obj : Cuisine
	 * @return state0 : boolean
	 */
	@Override
	public boolean delete(Cuisine obj) {
		final String SQL_DELETE = "DELETE FROM cuisine WHERE idCuisine=?";
		final String SQL_DELETE_LOCAL = "DELETE FROM local WHERE idLocal = ?";
		boolean state = false;
		boolean state0 = false;
		boolean state2 = false;
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_DELETE);
			stm.setInt(1, obj.getIdCuisine());
			
			int result = stm.executeUpdate();
			if (result > 0) {
				state = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_DELETE_LOCAL);
			stm.setInt(1, obj.getIdCuisine());
			
			int result = stm.executeUpdate();
			if (result > 0) {
				state2 = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(state && state2) {
			state0 = true;
		}
		return state0;
	}
	
	/**
	 * @brief permet d'obtenir le dernier idLocal de la table local.
	 * @details on l'utilise pour ajouter une cuisine à un local qui est ajouté dans un appartement.
	 * @return idLocal : Integer
	 */
	public int getLastIdLocal() {
		final String SQL_QUERY_ID = "SELECT MAX(idLocal) FROM local";
		int idLocal = 0;
		try {
			Statement stm = connect.createStatement();
			ResultSet rs = stm.executeQuery(SQL_QUERY_ID);
			if(rs.next()) {
				idLocal = rs.getInt(1);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return idLocal;
	}
	
}
