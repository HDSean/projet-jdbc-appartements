package controller;

import java.sql.Connection;

import model.DBConnexion;

/**
 * @file DAOFactory.java
 * @author Anica Sean & Ung Alexandre
 * @version 1.0
 * @date 17 Decembre 2020
 * @brief classe factory du pattern Factory permet d'encapsuler l'instanciation de nos objets dans une classe.
 * @detail les instanciations de DAO seront centralisés dans un seul objet. source : https://cyrille-herby.developpez.com/tutoriels/java/mapper-sa-base-donnees-avec-pattern-dao/#LII
 */
public class DAOFactory {
	protected static final Connection conn = DBConnexion.getInstance();
	
	public static AppartementDAO getAppartementDAO() {
		return new AppartementDAO(conn);
	}
	
	public static CuisineDAO getCuisineDAO() {
		return new CuisineDAO(conn);
	}
	
	public static ChambreDAO getChambreDAO() {
		return new ChambreDAO(conn);
	}
	
	public static SalleDeBainDAO getSalleDeBainDAO() {
		return new SalleDeBainDAO(conn);
	}
	
	public static LiaisonDAO getLiaisonDAO() {
		return new LiaisonDAO(conn);
	}
}
