package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import model.Liaison;

/**
 * @file LiaisonDAO.java
 * @author Anica Sean & Ung Alexandre
 * @version 2.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui permet de gérer les liaisons entre les locaux dans la bdd.
 * @details si on supprime un local on supprime toutes les liaisons ou il est présent de la bdd.
 */
public class LiaisonDAO  extends DAO<Liaison>{

	/**
	 * 
	 * @param conn : Connection
	 */
	public LiaisonDAO(Connection conn) {
		super(conn);
	}

	/**
	 * @brief permet de trouver une liaison en fonction de son id.
	 * @param id : Integer
	 * @return liaison : Liaison
	 */
	@Override
	public Liaison find(int id) {
		Liaison liaison = null;
		final String SQL_QUERY = "SELECT * FROM liaison WHERE idLiaison=?";
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_QUERY);
			stm.setInt(1, id);
			ResultSet result = stm.executeQuery();
			if (result.first()) {
				liaison = new Liaison(id, result.getInt("idAppartement"), 
						result.getInt("idLocal1"), 
						result.getInt("idLocal2"));
			}
		}catch (SQLException e) {
			e.printStackTrace();
		}
		return liaison;
	}

	/**
	 * @brief permet d'insérer une liaison dans la bdd à partir d'un objet liaison.
	 * @param obj : Liaison
	 * @return etat : boolean
	 */
	@Override
	public boolean insert(Liaison obj) {
		final String SQL_INSERT = "INSERT INTO Liaison VALUES(NULL, ?, ?, ?)";
		boolean etat = false;
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_INSERT);
			stm.setInt(1, obj.getIdAppartement());
			stm.setInt(2, obj.getIdLocal1());
			stm.setInt(3, obj.getIdLocal2());
			int result = stm.executeUpdate();
			if(result > 0) {
				etat = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return etat;
	}
	
	/**
	 * @brief permet de mettre à jour une liaison dans la bdd à partir d'un objet liaison.
	 * @param obj : Liaison
	 * @return state : boolean
	 */
	@Override
	public boolean update(Liaison obj) {
		final String SQL_UPDATE = "UPDATE Liaison SET idLiaison = ?, idAppartement=?, idLocal1=?, idLocal2=?)";
		boolean state = false;
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_UPDATE);
			stm.setInt(1, obj.getIdLiaison());
			stm.setInt(2, obj.getIdAppartement());
			stm.setInt(3, obj.getIdLocal1());
			stm.setInt(4, obj.getIdLocal2());
		
			int result = stm.executeUpdate();
			if(result > 0) {
				state = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return state;
	}

	/**
	 * @brief permet de supprimer une liaison de la bdd à partir d'un objet liaison.
	 * @param obj : Liaison
	 * @return state : boolean
	 */
	@Override
	public boolean delete(Liaison obj) {
		final String SQL_DELETE = "DELETE FROM Liaison WHERE idLiaison=?";
		boolean state = false;
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_DELETE);
			stm.setInt(1, obj.getIdLiaison());
			
			int result = stm.executeUpdate();
			if (result > 0) {
				state = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return state;
	}
	
	
}
