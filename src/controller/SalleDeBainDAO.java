package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.SalleDeBain;

/**
 * @file SalleDeBainDAO.java
 * @author Anica Sean & Ung Alexandre
 * @version 2.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui permet de gérer les salles de bain et donc les locaux qui en résultent dans la bdd.
 * @details si on supprime une salle de bain on supprime tous les locaux de la salle de bain.
 */
public class SalleDeBainDAO extends DAO<SalleDeBain>{

	/**
	 * 
	 * @param conn : Connection
	 */
	public SalleDeBainDAO(Connection conn) {
		super(conn);
	}
	
	/**
	 * @brief permet de retrouver une salle de bain en fonction de idSalleDeBain
	 * @param id : Integer
	 * @return sdb : SalleDeBain
	 */
	@Override
	public SalleDeBain find(int id) {
		SalleDeBain sdb=null;
		final String SQL_QUERY = "SELECT * FROM SalleDeBain WHERE idSalleDeBain=?";
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_QUERY);
			stm.setInt(1,id);
			ResultSet rs = stm.executeQuery();
			if(rs.first()) {
				sdb = new SalleDeBain(id,rs.getInt("nbPointsEau"),id,rs.getInt("idAppartement"), rs.getDouble("surface"), rs.getString("description"));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return sdb;
	}

	/**
	 * @brief permet d'insérer une salle de bain en fonction de l'objet SalleDeBain passé en paramètre
	 * @details on insère aussi le nouveau local créé en paramètre.
	 * @param obj : SalleDeBain
	 * @return etat : boolean
	 */
	@Override
	public boolean insert(SalleDeBain obj) {
		boolean etat = false;
		boolean etat1 = false;
		boolean etat2 = false;
		
		final String SQL_INSERT_LOCAL = "INSERT INTO local VALUES (NULL, ?, ?, ?)";
		/*on complète la table local en ajoutant les références de la salle de bain*/
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_INSERT_LOCAL);
			stm.setInt(1, obj.getIdAppartement());
			stm.setDouble(2, obj.getSurface());
			stm.setString(3, obj.getDescription());
			int result = stm.executeUpdate();
			if(result > 0) {
				etat1 = true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		obj.setIdLocal(getLastIdLocal());
		/*on complète la table salle de bain*/
		final String SQL_INSERT = "INSERT INTO SalleDeBain VALUES(?, ?, ?)";	
		try {
			PreparedStatement stm2 = connect.prepareStatement(SQL_INSERT);
			stm2.setInt(1, obj.getIdLocal());
			stm2.setInt(3, obj.getNbPointsEau());
			stm2.setInt(2, obj.getIdAppartement());
			int result = stm2.executeUpdate();
			if(result > 0) {
				etat2 = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(etat1 && etat2) {
			etat = true;
		}
		return etat;
	}

	/**
	 * @brief permet de mettre à jour les informations d'une salle de bain en fonction de l'objet SalleDeBain passé en paramètre
	 * @details on maj aussi le local associé .
	 * @param obj : SalleDeBain
	 * @return state : boolean
	 */
	@Override
	public boolean update(SalleDeBain obj) {
		final String SQL_UPDATE = "UPDATE SalleDeBain SET idAppartement=?, nbPointsEau=?)";
		boolean state = false;
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_UPDATE);
			stm.setInt(1, obj.getIdAppartement());
			stm.setInt(2, obj.getNbPointsEau());
			
			int result = stm.executeUpdate();
			if(result > 0) {
				state = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		boolean state2 = false;
		final String SQL_UPDATE_LOCAL = "UPDATE local SET idAppartement=?, surface=?, description=? WHERE idLocal=?";
		try {
			PreparedStatement stm2 = connect.prepareStatement(SQL_UPDATE_LOCAL);
			stm2.setInt(1, obj.getIdAppartement());
			stm2.setDouble(2, obj.getSurface());
			stm2.setString(3, obj.getDescription());
			stm2.setInt(4, obj.getIdSalleDeBain());//idLocal = idSalleDeBain
			int result = stm2.executeUpdate();
			if(result > 0) {
				state2 = true;
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		boolean state0 = false;
		if(state && state2) {
			state0 = true;
		}
		return state0;
	}

	/**
	 * @brief permet de supprimer une salle de bain en fonction de l'objet SalleDeBain passé en paramètre
	 * @details on supprime aussi le local associé .
	 * @param obj : SalleDeBain
	 * @return state0 : boolean
	 */
	@Override
	public boolean delete(SalleDeBain obj) {
		final String SQL_DELETE = "DELETE FROM SalleDeBain WHERE idSalleDeBain=?";
		final String SQL_DELETE_LOCAL = "DELETE FROM local WHERE idLocal = ?";
		boolean state = false;
		boolean state0 = false;
		boolean state2 = false;
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_DELETE);
			stm.setInt(1, obj.getIdSalleDeBain());
			
			int result = stm.executeUpdate();
			if (result > 0) {
				state = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_DELETE_LOCAL);
			stm.setInt(1, obj.getIdSalleDeBain());
			
			int result = stm.executeUpdate();
			if (result > 0) {
				state2 = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(state && state2) {
			state0 = true;
		}
		return state0;
	}
	
	/**
	 * @brief permet d'obtenir l'idLocal du dernier local créé dans la base de données.
	 * @details est utilisé dans la classe AppartmentDAO.
	 * @return idLocal : Integer
	 */
	public int getLastIdLocal() {
		final String SQL_QUERY_ID = "SELECT MAX(idLocal) FROM local";
		int idLocal = 0;
		try {
			Statement stm = connect.createStatement();
			ResultSet rs = stm.executeQuery(SQL_QUERY_ID);
			if(rs.next()) {
				idLocal = rs.getInt(1);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return idLocal;
	}

}
