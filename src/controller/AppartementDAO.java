package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import model.Appartement;
import model.Chambre;
import model.Cuisine;
import model.Liaison;
import model.Local;
import model.SalleDeBain;

/**
 * @file AppartementDAO.java
 * @author Anica Sean & Ung Alexandre
 * @version 3.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui permet de récupérer les données depuis
 * 		  la base de connées et de remplir les objets Appartement. 
 * 		  De plus On modifie les tables en cascades en fonction des clefs étrangères.
 * @details Cette classe hérite de la classe abstraite DAO, on redéfinit
 * 			toutes les fonctions de la classe mère en adaptant pour les appartements.
 */
public class AppartementDAO extends DAO<Appartement> {

	/**
	 * 
	 * @param conn : Connection
	 */
	public AppartementDAO(Connection conn) {
		super(conn);
	}

	/**
	 * @brief permet de retrouver un appartement à partir de l'idAppartement dans la bdd.
	 * @param idAppartement : Integer
	 * @return appartement : Appartement
	 */
	@Override
	public Appartement find(int idAppartement) {
		Appartement appartement = null;
		final String SQL_QUERY = "SELECT * FROM Appartement WHERE id=?";
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_QUERY);
			stm.setInt(1,idAppartement);
			ResultSet rs = stm.executeQuery();
			if(rs.first()) {
				appartement = new Appartement(idAppartement,rs.getString("adresse"), rs.getString("description"), rs.getBoolean("enConstruction"));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return appartement;
	}

	/**
	 * @brief permet d'insérer un Appartement dans la base de données
	 * @details on insère aussi les locaux de l'appartement, donc les cuisines, chambres, salles de bain.
	 * @param obj : Appartement
	 * @return etat0 : boolean
	 */
	@Override
	public boolean insert(Appartement obj) {
		final String SQL_INSERT = "INSERT INTO Appartement VALUES(NULL, ?, ?, ?)";
		boolean etat = false;
		boolean etat0 = false;
		boolean etat2 = false;
		boolean etat3 = false;
		boolean etat4 = false;
		boolean etat5 = false;
		
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_INSERT);
			stm.setString(1, obj.getAdresse());
			stm.setString(2, obj.getDescription());
			stm.setBoolean(3, obj.isEnConstruction());
			int result = stm.executeUpdate();
			if(result > 0) {
				etat = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		/*ajout des cuisines*/
		ArrayList<Cuisine> cuisineList = new ArrayList<>();
		cuisineList = obj.getCuisines();
		CuisineDAO cuisineDAO = new CuisineDAO(connect);
		for(Cuisine c: cuisineList) {
			c.setIdAppartement(getLastIdAppartement());
			etat2 = cuisineDAO.insert(c);
		}
		
		/*ajout des chambres*/
		ArrayList<Chambre> chambreList = new ArrayList<>();
		chambreList = obj.getChambres();
		ChambreDAO chambreDAO = new ChambreDAO(connect);
		for(Chambre c : chambreList) {
			c.setIdAppartement(getLastIdAppartement());
			etat3 = chambreDAO.insert(c);
		}
		
		/*ajout des salles de bain*/
		ArrayList<SalleDeBain> salleDeBainList = new ArrayList<>();
		salleDeBainList = obj.getSallesDeBain();
		SalleDeBainDAO salledebainDAO = new SalleDeBainDAO(connect);
		for(SalleDeBain s : salleDeBainList) {
			s.setIdAppartement(getLastIdAppartement());
			etat4 = salledebainDAO.insert(s);
		}
		/*ajout des liaisons*/
		ArrayList<Liaison> liaisonList = new ArrayList<>();
		liaisonList = obj.getLiaisons();
		LiaisonDAO liaisonDAO = new LiaisonDAO(connect);
		for(Liaison l : liaisonList) {
			l.setIdAppartement(getLastIdAppartement());
			etat5 = liaisonDAO.insert(l);
		}
		
		if(etat && etat2 && etat3 && etat4 && etat5) {
			etat0 = true;
		}
		return etat0;
	}

	/**
	 * @brief permet de mettre à jour l'adresse, la description et l'état d'un appartement.
	 * @param obj : Appartement
	 * @return state : boolean
	 */
	@Override
	public boolean update(Appartement obj) {
		final String SQL_UPDATE = "UPDATE Appartement SET adresse=?, description=?, enConstruction=?)";
		boolean state = false;
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_UPDATE);
			stm.setString(1, obj.getAdresse());
			stm.setString(2, obj.getDescription());
			stm.setBoolean(3, obj.isEnConstruction());
			
			int result = stm.executeUpdate();
			if(result > 0) {
				state = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return state;
	}

	/**
	 * @brief permet de supprimer un appartement de la base de données.
	 * @details supprimer aussi en cascade tous les locaux de celui-ci
	 * @param obj : Appartement
	 * @return stateFinal : boolean
	 */
	@Override
	public boolean delete(Appartement obj) {
		/*on supprime l'appartement*/
		final String SQL_DELETE = "DELETE FROM Appartement WHERE idAppartement=?";
		boolean stateAppartement = false;
		boolean stateLocaux = false;
		boolean stateFinal = false;
		boolean stateLiaison = false;
		PreparedStatement stm;
		
		try {
			stm = connect.prepareStatement(SQL_DELETE);
			stm.setInt(1, obj.getIdAppartement());
			
			int result = stm.executeUpdate();
			if (result > 0) {
				stateAppartement = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		/*on supprime les locaux de l'appartement*/
		try {
			/*suppression des cuisines*/
			stm = connect.prepareStatement("DELETE local,cuisine FROM local INNER JOIN cuisine ON local.idLocal = cuisine.idCuisine WHERE local.idAppartement = ?");
			stm.setInt(1, obj.getIdAppartement());
			int result = stm.executeUpdate();
			/*suppression des chambres*/
			stm = connect.prepareStatement("DELETE local,chambre FROM local INNER JOIN chambre ON local.idLocal = chambre.idChambre WHERE local.idAppartement = ?");
			stm.setInt(1, obj.getIdAppartement());
			int result2 = stm.executeUpdate();
			/*suppression des salles de bain*/
			stm = connect.prepareStatement("DELETE local,salledebain FROM local INNER JOIN salledebain ON local.idLocal = salledebain.idSalleDeBain WHERE local.idAppartement = ?");
			stm.setInt(1, obj.getIdAppartement());
			int result3 = stm.executeUpdate();
			if(result>0 && result2>0 && result3>0) {
				stateLocaux = true;
			}
		}catch(SQLException e2) {
			e2.printStackTrace();
		}
		
		/*on supprime les liaisons entre les locaux*/
		try {
			stm = connect.prepareStatement("DELETE liaison FROM liaison INNER JOIN appartement ON appartement.idAppartement = liaison.idAppartement WHERE local.idAppartement = ?");
			stm.setInt(1, obj.getIdAppartement());
			int result = stm.executeUpdate();
			if(result>0 ) {
				stateLiaison = true;
			}
		}catch(SQLException e3) {
			e3.printStackTrace();
		}
		
		if(stateAppartement && stateLocaux && stateLiaison) {
			/*cela signifie qu'on a supprimé l'appartement, tous les locaux qui étaient reliés à lui et toutes les liaisons de l'appartement.*/
			stateFinal = true;
		}
		return stateFinal;
	}
	
	/**
	 * @brief permet d'obtenir toutes les cuisines de l'appartement dont l'id est idAppartement.
	 * @param idAppartement : Integer
	 * @return cuisineList : ArrayList<Cuisine>
	 */
	public ArrayList<Cuisine> getCuisinesByApt(int idAppartement){
		ArrayList<Cuisine> cuisineList = new ArrayList<Cuisine>();
		final String SQL_QUERY_CUISINE = "SELECT * FROM local INNER JOIN cuisine ON local.idLocal=cuisine.idCuisine WHERE local.idAppartement=?";
		try {
			PreparedStatement stm;
			stm = connect.prepareStatement(SQL_QUERY_CUISINE);
			stm.setInt(1, idAppartement);
			ResultSet rs = stm.executeQuery();
			while(rs.next()) {
				cuisineList.add(new Cuisine(rs.getInt("idCuisine"), rs.getInt("nbPointsGaz"), rs.getInt("idLocal"), rs.getInt("idAppartement"), rs.getDouble("surface"), rs.getString("description")));
			}
		}catch(SQLException e3) {
			e3.printStackTrace();
		}
		return cuisineList;
	}
	
	/**
	 * @brief permet d'obtenir toutes les chambres de l'appartement dont l'id est idAppartement.
	 * @param idAppartement : Integer
	 * @return chambresList : ArrayList<Chambre>
	 */
	public ArrayList<Chambre> getChambresByApt(int idAppartement){
		ArrayList<Chambre> chambresList = new ArrayList<Chambre>();
		final String SQL_QUERY_CHAMBRE = "SELECT * FROM local INNER JOIN chambre ON local.idLocal=chambre.idChambre WHERE local.idAppartement=?";
		try {
			PreparedStatement stm;
			stm = connect.prepareStatement(SQL_QUERY_CHAMBRE);
			stm.setInt(1, idAppartement);
			ResultSet rs = stm.executeQuery();
			while(rs.next()) {
				chambresList.add(new Chambre(rs.getInt("idChambre"), rs.getString("typeChambre"), rs.getInt("idLocal"), rs.getInt("idAppartement"), rs.getDouble("surface"), rs.getString("description")));
			}
		}catch(SQLException e3) {
			e3.printStackTrace();
		}
		return chambresList;
	}
	
	/**
	 * @brief permet d'obtenir toutes les salles de bain de l'appartement dont l'id est idAppartement.
	 * @param idAppartement : Integer
	 * @return salleDeBainList : ArrayList<Chambre>
	 */
	public ArrayList<SalleDeBain> getSallesDeBainByApt(int idAppartement){
		ArrayList<SalleDeBain> salleDeBainList = new ArrayList<SalleDeBain>();
		final String SQL_QUERY_SALLESDEBAIN = "SELECT * FROM local INNER JOIN salledebain ON local.idLocal=salledebain.idSalleDeBain WHERE local.idAppartement=?";
		try {
			PreparedStatement stm;
			stm = connect.prepareStatement(SQL_QUERY_SALLESDEBAIN);
			stm.setInt(1, idAppartement);
			ResultSet rs = stm.executeQuery();
			while(rs.next()) {
				salleDeBainList.add(new SalleDeBain(rs.getInt("idSalleDeBain"), rs.getInt("nbPointsEau"), rs.getInt("idLocal"), rs.getInt("idAppartement"), rs.getDouble("surface"), rs.getString("description")));
			}
		}catch(SQLException e3) {
			e3.printStackTrace();
		}
		return salleDeBainList;
	}
	
	/**
	 * @brief permet d'obtenir tous les objets Appartement de la base de données.
	 * @details Cette liste est donnée sous forme d'ArrayList
	 * @return appartementList : ArrayList<Appartement>
	 */
	public ArrayList<Appartement> getAllAppartement(){
		ArrayList<Appartement> appartementList = new  ArrayList<Appartement>();
		final String SQL_QUERY = "SELECT * FROM Appartement";
		
		try {
			Statement stm = connect.createStatement();
			ResultSet rs = stm.executeQuery(SQL_QUERY);
			while(rs.next()) {
				Appartement appartement = new Appartement(rs.getInt("idAppartement"), rs.getString("adresse"), rs.getString("description"), rs.getBoolean("enConstruction"));
				appartement.setCuisines(getCuisinesByApt(appartement.getIdAppartement()));
				appartement.setChambres(getChambresByApt(appartement.getIdAppartement()));
				appartement.setSallesDeBain(getSallesDeBainByApt(appartement.getIdAppartement()));
				appartementList.add(appartement);	
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return appartementList;
	}
	
	/**
	 * @brief permet d'obtenir tous les locaux en fonction de leurs appartements respectifs
	 * @details utilisé pour l'affichage des apts et des locaux.
	 * @return localList : ArrayList<Local>
	 */
	public ArrayList<Local> getLocauxByAppartement() {
		ArrayList<Local> locallist = new ArrayList<Local>();
		final String SQL_QUERY = "SELECT * FROM local ORDER BY idAppartement";
		try {
			Statement stm = connect.createStatement();
			ResultSet rs = stm.executeQuery(SQL_QUERY);
			while(rs.next()) {
				locallist.add(new Local(rs.getInt("idLocal"), rs.getInt("IdAppartement"), rs.getDouble("surface"), rs.getString("description")));
			}
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return locallist;
	}
	
	/**
	 * @brief permet de changer l'état d'un appartement de 1 pour en construction, 0 sinon.
	 * @param idAppartement : Integer
	 * @param etat : boolean
	 * @return true si la fonction s'effectue correctement.
	 */
	public boolean changerEtat(int idAppartement, boolean etat) {
		PreparedStatement ps;
		try {
			ps = connect.prepareStatement("UPDATE appartement SET enConstruction=? WHERE idAppartement=?",Statement.RETURN_GENERATED_KEYS);
			ps.setBoolean(1, etat);
			ps.setInt(2, idAppartement);
			ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;	
	}
	
	/**
	 * @brief trouver tous les appartements dont la surface est supérieur ou égale à la donnée entrée en paramètre.
	 * @param surface : double
	 * @return ap : ArrayList<Appartement>
	 */
	public ArrayList<Appartement> findAppartementsBySurface(double surface){
		ArrayList<Appartement> ap = new ArrayList<Appartement>();
		Statement stm;
		try {
			stm = connect.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM appartement a WHERE(SELECT SUM(surface) FROM local l WHERE l.idAppartement = a.idAppartement) >=" + surface );
			while(rs.next()) {
				ap.add(new Appartement(rs.getInt("idAppartement"), rs.getString("adresse"), rs.getString("description"), rs.getBoolean("enConstruction")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ap;
	}
	
	/**
	 * @brief trouve les appartements qui ont au moins nbSallesDeBain salles de bain
	 * @param nbSallesDeBain : Integer
	 * @return ap : ArrayList<Appartement>
	 */
	public ArrayList<Appartement> findAppartementsByNbSallesDeBain(int nbSallesDeBain){
		ArrayList<Appartement> ap = new ArrayList<Appartement>();
		Statement stm;
		try {
			stm = connect.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM appartement a WHERE(SELECT COUNT(*) FROM salledebain s WHERE a.idAppartement=s.idAppartement) >=" + nbSallesDeBain );
			while(rs.next()) {
				ap.add(new Appartement(rs.getInt("idAppartement"), rs.getString("adresse"), rs.getString("description"), rs.getBoolean("enConstruction")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ap;
	}
	
	/**
	 * @brief trouve les appartements qui ont nbCuisines cuisines
	 * @param nbCuisines : Integer
	 * @return ap : ArrayList<Appartement>
	 */
	public ArrayList<Appartement> findAppartementsByNbCuisines(int nbCuisines){
		ArrayList<Appartement> ap = new ArrayList<Appartement>();
		Statement stm;
		try {
			stm = connect.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM appartement a WHERE(SELECT COUNT(*) FROM cuisine c WHERE a.idAppartement=c.idAppartement) >=" + nbCuisines );
			while(rs.next()) {
				ap.add(new Appartement(rs.getInt("idAppartement"), rs.getString("adresse"), rs.getString("description"), rs.getBoolean("enConstruction")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ap;
	}
	
	/**
	 * @brief trouve les appartements qui ont au moins nbChambres chambres
	 * @param nbChambres : Integer
	 * @return ap : ArrayList<Appartement>
	 */
	public ArrayList<Appartement> findAppartementsByNbChambres(int nbChambres){
		ArrayList<Appartement> ap = new ArrayList<Appartement>();
		Statement stm;
		try {
			stm = connect.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM appartement a WHERE(SELECT COUNT(*) FROM chambre c WHERE a.idAppartement=c.idAppartement) >=" + nbChambres );
			while(rs.next()) {
				ap.add(new Appartement(rs.getInt("idAppartement"), rs.getString("adresse"), rs.getString("description"), rs.getBoolean("enConstruction")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ap;
	}
	
	/**
	 * @brief trouve les appartement qui ont des cuisines ayant au moins nbPointsGaz
	 * @param nbPointsGaz : Integer
	 * @return ap : ArrayList<Appartement>
	 */
	public ArrayList<Appartement> findAppartementByPointsGaz(int nbPointsGaz){
		ArrayList<Appartement> ap = new ArrayList<Appartement>();
		Statement stm;
		try {
			stm = connect.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM appartement a WHERE(SELECT COUNT(*) FROM cuisine c WHERE a.idAppartement=c.idAppartement AND nbPointsGaz >=" + nbPointsGaz + ")" );
			while(rs.next()) {
				ap.add(new Appartement(rs.getInt("idAppartement"), rs.getString("adresse"), rs.getString("description"), rs.getBoolean("enConstruction")));
				
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ap;
	}
	
	/**
	 * @brief trouve les appartement qui ont des salles de bain ayant au moins nbPointsEau
	 * @param nbPointsEau : Integer
	 * @return ap : ArrayList<Appartement>
	 */
	public ArrayList<Appartement> findAppartementByPointsEau(int nbPointsEau){
		ArrayList<Appartement> ap = new ArrayList<Appartement>();
		Statement stm;
		try {
			stm = connect.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM appartement a WHERE(SELECT COUNT(*) FROM salledebain s WHERE a.idAppartement=s.idAppartement AND nbPointsEau >=" + nbPointsEau + ")" );
			while(rs.next()) {
				ap.add(new Appartement(rs.getInt("idAppartement"), rs.getString("adresse"), rs.getString("description"), rs.getBoolean("enConstruction")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ap;
	}
	
	/**
	 * @brief permet de trouver les appartements dont le type de chambre est typeChambre.
	 * @param typeChambre : String
	 * @return ap : ArrayList<Appartement>
	 */
	public ArrayList<Appartement> findAppartementByTypeChambre(String typeChambre){
		ArrayList<Appartement> ap = new ArrayList<Appartement>();
		Statement stm;
		try {
			stm = connect.createStatement();
			ResultSet rs = stm.executeQuery("SELECT * FROM appartement a WHERE(SELECT COUNT(*) FROM chambre c WHERE c.idAppartement = a.idAppartement AND typeChambre ='" + typeChambre +"')" );
			while(rs.next()) {
				ap.add(new Appartement(rs.getInt("idAppartement"), rs.getString("adresse"), rs.getString("description"), rs.getBoolean("enConstruction")));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return ap;
	}
	
	
	/**
	 * @brief permet d'obtenir l'id du dernier appartemenbt ajouté dans la base de données.
	 * @detaisl utilisé pour ajouter des appartement dans la base ainsi que leur locaux.
	 * @return idAppartement : Integer
	 */
	public int getLastIdAppartement() {
		final String SQL_QUERY_ID = "SELECT MAX(idAppartement) FROM appartement";
		int idAppartement = 0;
		try {
			Statement stm = connect.createStatement();
			ResultSet rs = stm.executeQuery(SQL_QUERY_ID);
			if(rs.next()) {
				idAppartement = rs.getInt(1);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return idAppartement;
	}
	
	
	


}
