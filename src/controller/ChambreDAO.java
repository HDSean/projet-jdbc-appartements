package controller;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import model.Chambre;

/**
 * @file ChambreDAO.java
 * @author Anica Sean & Ung Alexandre
 * @version 3.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui permet de récupérer les données depuis
 * 		  la base de connées et de remplir les objets Chambre ainsi que la table local.
 * 		  De plus On modifie les tables en cascades en fonction des clefs étrangères.
 * @details Cette classe hérite de la classe abstraite DAO, on redéfinit
 * 			toutes les fonctions de la classe mère en adaptant pour les chambres.
 */
public class ChambreDAO extends DAO<Chambre>{

	/**
	 * 
	 * @param conn : Connection
	 */
	public ChambreDAO(Connection conn) {
		super(conn);
	}

	/**
	 * @brief permet de retrouver une chambre en fonction de idChambre
	 * @param id : Integer
	 * @return chambre : Chambre
	 */
	@Override
	public Chambre find(int id) {
		Chambre chambre=null;
		final String SQL_QUERY = "SELECT * FROM Chambre WHERE idChambre=?";
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_QUERY);
			stm.setInt(1,id);
			ResultSet rs = stm.executeQuery();
			if(rs.first()) {
				chambre = new Chambre(id,rs.getString("typeChambre"),id,rs.getInt("idAppartement"), rs.getDouble("surface"), rs.getString("description"));
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return chambre;
	}

	/**
	 * @brief permet d'insérer une chambre dans la base de données en l'ajoutant dans les tables
	 * 		  chambres et local.
	 * @param obj : Chambre
	 * @return etat : boolean
	 */
	@Override
	public boolean insert(Chambre obj) {
		
		boolean etat = false;
		boolean etat1 = false;
		boolean etat2 = false;
		final String SQL_INSERT_LOCAL = " INSERT INTO local VALUES (NULL, ?, ?, ?)";
		/*on complète la table local en ajoutant les références de la chambre*/
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_INSERT_LOCAL);
			stm.setInt(1, obj.getIdAppartement());
			stm.setDouble(2, obj.getSurface());
			stm.setString(3, obj.getDescription());
			int result = stm.executeUpdate();
			if(result > 0) {
				etat1 = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		obj.setIdLocal(getLastIdLocal());
		/*on complète la table chambre*/
		final String SQL_INSERT = "INSERT INTO Chambre VALUES(?, ?, ?)";
		try {
			PreparedStatement stm2 = connect.prepareStatement(SQL_INSERT);
			stm2.setInt(1, obj.getIdLocal());
			stm2.setString(3, obj.getTypeChambre());
			stm2.setInt(2, obj.getIdAppartement());
			int result = stm2.executeUpdate();
			if(result > 0) {
				etat2 = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		if(etat1 && etat2) {
			etat = true;
		}
		return etat;
	}

	/**
	 * @brief permet de mettre à jour les information d'une chambre dans la base de données en modifiant dans les tables
	 * 		  chambres et local.
	 * @param obj : Chambre
	 * @return state0 : boolean
	 */
	@Override
	public boolean update(Chambre obj) {
		final String SQL_UPDATE = "UPDATE Chambre SET idAppartement=?, typeChambre=?)";
		boolean state = false;
		
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_UPDATE);
			stm.setInt(1, obj.getIdAppartement());
			stm.setString(2, obj.getTypeChambre());
			
			int result = stm.executeUpdate();
			if(result > 0) {
				state = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		boolean state2 = false;
		final String SQL_UPDATE_LOCAL = "UPDATE local SET idAppartement=?, surface=?, description=? WHERE idLocal=?";
		try {
			PreparedStatement stm2 = connect.prepareStatement(SQL_UPDATE_LOCAL);
			stm2.setInt(1, obj.getIdAppartement());
			stm2.setDouble(2, obj.getSurface());
			stm2.setString(3, obj.getDescription()); 
			stm2.setInt(4, obj.getIdChambre());//idChambre et idLocal sont les mêmes
			int result = stm2.executeUpdate();
			if(result > 0) {
				state2 = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		boolean state0 = false;
		if(state && state2) {
			state0 = true;
		}
		return state0;
	}

	/**
	 * @brief permet de supprimer une chambre de la base de données en la supprimant  des tables
	 * 		  chambres et local.
	 * @param obj : Chambre
	 * @return state0 : boolean
	 */
	@Override
	public boolean delete(Chambre obj) {
		final String SQL_DELETE = "DELETE FROM Chambre WHERE idChambre=?";
		final String SQL_DELETE_LOCAL = "DELETE FROM local WHERE idLocal = ?";
		boolean state = false;
		boolean state0 = false;
		boolean state2 = false;
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_DELETE);
			stm.setInt(1, obj.getIdChambre());
			
			int result = stm.executeUpdate();
			if (result > 0) {
				state = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		try {
			PreparedStatement stm = connect.prepareStatement(SQL_DELETE_LOCAL);
			stm.setInt(1, obj.getIdChambre());
			
			int result = stm.executeUpdate();
			if (result > 0) {
				state2 = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		if(state && state2) {
			state0 = true;
		}
		return state0;
	}
	
	/**
	 * @brief permet d'obtenir le dernier idLocal de la table local.
	 * @details on l'utilise pour ajouter une chambre à un local qui est ajouté dans un appartement.
	 * @return idLocal : Integer
	 */
	public int getLastIdLocal() {
		final String SQL_QUERY_ID = "SELECT MAX(idLocal) FROM local";
		int idLocal = 0;
		try {
			Statement stm = connect.createStatement();
			ResultSet rs = stm.executeQuery(SQL_QUERY_ID);
			if(rs.next()) {
				idLocal = rs.getInt(1);
			}
			
		}catch(SQLException e) {
			e.printStackTrace();
		}
		return idLocal;
	}
}
