package controller;

import java.sql.Connection;

/**
 * @file DAO.java
 * @author Anica Sean & Ung Alexandre
 * @version 3.0
 * @date 17 Decembre 2020
 * @brief Définit la classe abstraite DAO qui représente le patron de conception qui
 * 		  permet de compléter le modèle MVC et de regrouper l'accès aux données persistantes quelque part
 * 		  et non dans les classes métiers pour ne pas surcharger celles-ci, et intéragir avec la base de données.
 * @details Cette classe prend un argument T qui représente l'objet qu'on va associer aux tables de la bdd.
 */
public abstract class DAO<T> {
	protected Connection connect = null;
	
	/**
	 * @brief constructeur de la classe DAO permet d'indiquer la connexion à la base de données
	 * @param conn : Connection
	 */
	public DAO(Connection conn){
	    this.connect = conn;
	  }
	
	/**
	 * @brief méthode abstraite du DAO qui permet de retrouver des données dans la base
	 * @param id : Integer
	 * @return
	 */
	public abstract T find(int id);
	
	/**
	 * @brief méthode abstraite du DAO qui permet d'insérer des données dans la base
	 * @param obj : T
	 * @return
	 */
	public abstract boolean insert(T obj);
	
	/**
	 * @brief méthode abstraite du DAO qui permet de mettre à jour des données dans la base
	 * @param obj : T
	 * @return
	 */
	public abstract boolean update(T obj);
	
	/**
	 * @brief méthode abstraite du DAO qui permet de supprimer des données de la base
	 * @param obj : T
	 * @return
	 */
	public abstract boolean delete(T obj);
}
