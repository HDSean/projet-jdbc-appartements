package main;

import view.MainView;

/**
 * @file Main.java
 * @author Anica Sean & Ung Alexandre
 * @version 2.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui permet lancer l'application.
 * @details on y lance la vue qui est dans MainView.java
 */
public class Main {

	public static void main(String[] args) {
		MainView mainView = new MainView();
		mainView.start();
	}
}
