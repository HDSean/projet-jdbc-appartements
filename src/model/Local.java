package model;

/**
 * @file Local.java
 * @author Anica Sean & Ung Alexandre
 * @version 1.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui représente un objet Local et les fonctions basiques pour le manipuler.
 */
public class Local {
	private int idLocal;
	private int idAppartement;
	private double surface;
	private String description;
	
	/**
	 * @brief constructeur principal de la classe
	 * @param idLocal : Integer
	 * @param idAppartement : Integer
	 * @param surface : double
	 * @param description : String
	 */
	public Local(int idLocal, int idAppartement, double surface, String description){
		this.setIdLocal(idLocal);
		this.setIdAppartement(idAppartement);
		this.setSurface(surface);
		this.setDescription(description);
	}

	/**
	 * @brief getter de l'attribut idLocal
	 * @return idLocal : Integer
	 */
	public int getIdLocal() {
		return idLocal;
	}
	
	/**
	 * @brief setter de l'attribut idLocal
	 * @param idLocal : Integer
	 */
	public void setIdLocal(int idLocal) {
		this.idLocal = idLocal;
	}

	/**
	 * @brief getter de l'attribut idAppartement
	 * @return idAppartement : Integer
	 */
	public int getIdAppartement() {
		return idAppartement;
	}

	/**
	 * @brief setter de l'attribut idAppartement 
	 * @param idAppartement : Integer
	 */
	public void setIdAppartement(int idAppartement) {
		this.idAppartement = idAppartement;
	}

	/**
	 * @brief getter de l'attribut surface
	 * @return surface : double
	 */
	public double getSurface() {
		return surface;
	}

	/**
	 * @brief setter de l'attribut surface
	 * @param surface : double
	 */
	public void setSurface(double surface) {
		this.surface = surface;
	}

	/**
	 * @brief getter de l'attribut description
	 * @return description : String
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @brief settter de l'attribut description
	 * @param description : String
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	
	/**
	 * @brief surcharge de la fonction toString pour afficher les attributs de la classe Local
	 */
	@Override
	public String toString() {
		return String.format("Local[idLocal=%d, idAppartement=%s, Surface=%f m2, Description=%s]", 
				this.getIdLocal(), this.getIdAppartement(), this.getSurface(), this.getDescription());
	}
}
