package model;

/**
 * @file Appartement.java
 * @author Anica Sean & Ung Alexandre
 * @version 1.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui représente un objet appartement et les fonctions basiques pour les manipuler.
 */
public class Cuisine extends Local{
	
	private int idCuisine;
	private int nbPointsGaz;
	
	/**
	 * @brief constructeur principal de la classe
	 * @param idCuisine : Integer
	 * @param nbPointsGaz : Integer
	 * @param idLocal : Integer
	 * @param idAppartement : Integer
	 * @param surface : double
	 * @param description : String
	 */
	public Cuisine(int idCuisine, int nbPointsGaz, int idLocal, int idAppartement, double surface, String description) {
		super(idLocal, idAppartement, surface, description);
		this.setIdCuisine(idCuisine);
		this.setNbPointsGaz(nbPointsGaz);
	}

	/**
	 * @brief getter de idCuisine
	 * @return idCuisine : Integer
	 */
	public int getIdCuisine() {
		return idCuisine;
	}

	/**
	 * @brief setter de idCuisine
	 * @param idCuisine : Integer
	 */
	public void setIdCuisine(int idCuisine) {
		this.idCuisine = idCuisine;
	}

	/**
	 * @brief getter de nbPointsGaz
	 * @return nbPointsGaz : Integer
	 */
	public int getNbPointsGaz() {
		return nbPointsGaz;
	}

	/**
	 * @brief setter de nbPointsGaz
	 * @param nbPointsGaz : Integer
	 */
	public void setNbPointsGaz(int nbPointsGaz) {
		this.nbPointsGaz = nbPointsGaz;
	}
	
	/**
	 * @brief surcharge de la classe toString pour l'affichage des attributs de la classe Cuisine
	 */
	@Override
	public String toString() {
		return String.format("\n\t\tLocal[idCuisine : %d, nbPointsGaz : %d, idLocal : %d, idAppartement : %s, Surface : %f m2, Description : %s]",
				this.getIdCuisine(), this.getNbPointsGaz(), this.getIdLocal(), this.getIdAppartement(), this.getSurface(), this.getDescription());
	}

}
