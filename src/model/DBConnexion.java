package model;


import java.sql.*;

/**
 * @file Appartement.java
 * @author Anica Sean & Ung Alexandre
 * @version 1.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui permet de faire la connexion à la base de données.
 */
public class DBConnexion {
	private String url = "jdbc:mysql://localhost:3306/tpnote?zeroDateTimeBehavior=CONVERT_TO_NULL&serverTimezone=UTC";
	private String user = "root";
	private String passwd = "";
	private static Connection conn;
	
	/**
	 * @brief on se connecte à la bdd
	 */
	private DBConnexion() {
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				conn = DriverManager.getConnection(url, user, passwd);
			} catch (SQLException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
	}
	
	/**
	 * @brief getter de l'instance de connexion
	 * @return conn : Connection
	 */
	public static Connection getInstance() {
		if (conn == null) {
			new DBConnexion();
		}
		return conn;
	}
}
