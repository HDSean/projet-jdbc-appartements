package model;

/**
 * @file Liaison.java
 * @author Anica Sean & Ung Alexandre
 * @version 1.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui représente un objet Liaison et les fonctions basiques pour les manipuler.
 * @details représente la liaison entre deux locaux.
 */
public class Liaison {
    private int idLiaison;
    private int idAppartement;
    private int idLocal1;
    private int idLocal2;
    
    /**
     * @brief constructeur principal de la classe
     * @param idliaison : Integer
     * @param idappartement : Integer
     * @param idlocal1 : Integer
     * @param idlocal2 : Integer
     */
    public Liaison(int idliaison,int idappartement,int idlocal1,int idlocal2) {
        setIdLiaison(idliaison);
        setIdAppartement(idappartement);
        setIdLocal1(idlocal1);
        setIdLocal2(idlocal2);
    }

    /**
     * @brief getter de IdLiaison
     * @return idLiaison : Integer
     */
    public int getIdLiaison() {
        return idLiaison;
    }

    /**
     * @brief setter de idLiaison
     * @param idLiaison : Integer
     */
    public void setIdLiaison(int idLiaison) {
        this.idLiaison = idLiaison;
    }

    /**
     * @brief getter de idAppartement
     * @return idAppartement : Integer
     */
    public int getIdAppartement() {
        return idAppartement;
    }

    /**
     * @brief setter de idAppartement 
     * @param idAppartement : Integer
     */
    public void setIdAppartement(int idAppartement) {
        this.idAppartement = idAppartement;
    }

    /**
     * @brief getter de idLocal1
     * @return idLocal1 : Integer
     */
    public int getIdLocal1() {
        return idLocal1;
    }

    /**
     * @brief setter de idLocal1
     * @param idLocal1 : Integer
     */
    public void setIdLocal1(int idLocal1) {
        this.idLocal1 = idLocal1;
    }

    /**
     * @brief getter de idLocal2
     * @return idLocal2 : Integer
     */
    public int getIdLocal2() {
        return idLocal2;
    }

    /**
     * @brief setter de l'idLocal2
     * @param idLocal2 : Integer
     */
    public void setIdLocal2(int idLocal2) {
        this.idLocal2 = idLocal2;
    }
}