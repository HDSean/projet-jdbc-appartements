package model;


/**
 * @file SalleDeBain.java
 * @author Anica Sean & Ung Alexandre
 * @version 1.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui représente un objet SalleDeBain et les fonctions basiques pour les manipuler.
 * @details hérite de la classe Local
 */
public class SalleDeBain extends Local {
	private int idSalleDeBain;
	private int nbPointsEau;
	
	/**
	 * @brief constructeur de la classe salle de bain qui hérite du constructeur de la mère
	 * @param idSalleDeBain : Integer
	 * @param nbPointsEau : Integer
	 * @param idLocal : Integer
	 * @param idAppartement : Integer
	 * @param surface : double
	 * @param description ! String
	 */
	public SalleDeBain(int idSalleDeBain, int nbPointsEau, int idLocal, int idAppartement, double surface, String description) {
		super(idLocal, idAppartement, surface, description);
		this.setIdSalleDeBain(idSalleDeBain);
		this.setNbPointsEau(nbPointsEau);
	}
	
	/**
	 * @brief getter de l'attribut idSalleDeBain
	 * @return idSalleDeBain : Integer
	 */
	public int getIdSalleDeBain() {
		return idSalleDeBain;
	}
	
	/**
	 * @brief setter de l'attribut idSalleDeBain
	 * @param idSalleDeBain : Integer
	 */
	public void setIdSalleDeBain(int idSalleDeBain) {
		this.idSalleDeBain = idSalleDeBain;
	}
	
	/**
	 * @brief getter de l'attribut nbPointsEau
	 * @return nbPointsEau : Integer
	 */
	public int getNbPointsEau() {
		return nbPointsEau;
	}
	
	/**
	 * @brief setter de l'attribut nbPointsEau
	 * @param nbPointsEau
	 */
	public void setNbPointsEau(int nbPointsEau) {
		this.nbPointsEau = nbPointsEau;
	}
	
	/**
	 * @brief surcharge de la fonction toString pour afficher les attributs d'une salle de bain
	 */
	@Override
	public String toString() {
		return String.format("\n\t\tLocal[idSalleDeBain : %d, nbPointsEau : %d, idLocal : %d, idAppartement : %s, Surface : %f m2, Description : %s]",
				this.getIdSalleDeBain(), this.getNbPointsEau(), this.getIdLocal(), this.getIdAppartement(), this.getSurface(), this.getDescription());
	}
	
}
