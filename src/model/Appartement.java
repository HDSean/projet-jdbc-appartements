package model;

import java.util.ArrayList;

/**
 * @file Appartement.java
 * @author Anica Sean & Ung Alexandre
 * @version 1.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui représente un objet appartement et les fonctions basiques pour les manipuler.
 */
public class Appartement {
	private  int idAppartement; //autoIncrément donc pas besoin de setter
	private String Adresse;
	private String Description;
	private boolean enConstruction;
	
	private ArrayList<Cuisine> cuisines;
	private ArrayList<SalleDeBain> sallesDeBain;
	private ArrayList<Chambre> chambres;
	private ArrayList<Liaison> liaisons;
	
	/**
	 * @brief constructeur de la classe Appartement
	 * @param idappartement : Integer
	 * @param adresse : String
	 * @param description : String
	 * @param enconstruction : boolean
	 */
	public Appartement(int idappartement, String adresse, String description, 
			boolean enconstruction) {
		this.idAppartement = idappartement;
		this.setAdresse(adresse);
		this.setDescription(description);
		this.setEnConstruction(enconstruction);
		this.cuisines = new ArrayList<Cuisine>();
		this.chambres = new ArrayList<Chambre>();
		this.sallesDeBain = new ArrayList<SalleDeBain>();
		this.setLiaisons(new ArrayList<Liaison>());
	}

	/**
	 * @brief getter de l'attribut idAppartement
	 * @return idAppartement : Integer
	 */
	public int getIdAppartement() {
		return idAppartement;
	}

	/**
	 * @brief getter de l'attribut adresse
	 * @return Adresse : String
	 */
	public String getAdresse() {
		return Adresse;
	}

	/**
	 * @brief setter de l'attribut adresse
	 * @param adresse : String
	 */
	public void setAdresse(String adresse) {
		Adresse = adresse;
	}


	/**
	 * @brief getter de l'attribut description
	 * @return Description : String 
	 */
	public String getDescription() {
		return Description;
	}
	
	/**
	 * @brief setter de l'attribut description
	 * @param description : String
	 */
	public void setDescription(String description) {
		Description = description;
	}

	/**
	 * @brief getter de l'attribut enConstruction
	 * @return enConstruction : boolean
	 */
	public boolean isEnConstruction() {
		return enConstruction;
	}

	/**
	 * @brief setter de l'attribut en construction
	 * @param enConstruction : boolean
	 */
	public void setEnConstruction(boolean enConstruction) {
		this.enConstruction = enConstruction;
	}

	/**
	 * @brief getter de Cuisines
	 * @return cuisines : ArrayList<Cuisine>
	 */
	public ArrayList<Cuisine> getCuisines() {
		return cuisines;
	}

	/**
	 * @brief setter de Cuisines
	 * @param cuisines : ArrayList<Cuisine>
	 */
	public void setCuisines(ArrayList<Cuisine> cuisines) {
		this.cuisines = cuisines;
	}

	/**
	 * @brief getter de salles de bain
	 * @return SallesDeBain : ArrayList<SalleDeBain>
	 */
	public ArrayList<SalleDeBain> getSallesDeBain() {
		return sallesDeBain;
	}

	/**
	 * @brief setter de salles de bain
	 * @param sallesDeBain : ArrayList<SalleDeBain>
	 */
	public void setSallesDeBain(ArrayList<SalleDeBain> sallesDeBain) {
		this.sallesDeBain = sallesDeBain;
	}

	/**
	 * @brief getter de chambres
	 * @return chambres : ArrayList<Chambre>
	 */
	public ArrayList<Chambre> getChambres() {
		return chambres;
	}

	/**
	 * @brief setter de chambres
	 * @param chambres : ArrayList<Chambre>
	 */
	public void setChambres(ArrayList<Chambre> chambres) {
		this.chambres = chambres;
	}
	
	/**
	 * @brief ajouter une cuisine à la liste des cuisines
	 * @param cuisine : ArrayList<Cuisine>
	 */
	public void addCuisine(Cuisine cuisine) {
		cuisines.add(cuisine);
	}
	
	/**
	 * @brief ajouter une chambre à la liste des chambres
	 * @param chambre : ArrayList<Chambre>
	 */
	public void addChambre(Chambre chambre) {
		chambres.add(chambre);
	}
	
	/**
	 * @brief ajouter une salle de bain à la liste des salles de bain
	 * @param salledebain : ArrayList<SalleDeBain>
	 */
	public void addSalleDeBain(SalleDeBain salledebain) {
		sallesDeBain.add(salledebain);
	}
	
	/**
	 * @brief getter de liaisons
	 * @return liaisons : ArrayList<Liaison>
	 */
	public ArrayList<Liaison> getLiaisons() {
		return liaisons;
	}

	/**
	 * @brief setter de liaisons
	 * @param liaisons : ArrayList<Liaison>
	 */
	public void setLiaisons(ArrayList<Liaison> liaisons) {
		this.liaisons = liaisons;
	}
	
	/**
	 * @brief surcharge de la méthode toString pour afficher les infos d'un appartement.
	 * @return String 
	 */
	@Override
	public String toString() {
		String ETAT = "en construction";
		if(this.enConstruction == false) {
			ETAT = "prêt pour la livraison";
		}
		return "Appartement [idAppartement : " + this.getIdAppartement() + 
				", Adresse : " + this.getAdresse() + 
				", " + "Description : " + this.getDescription()  +
				", " + "enConstruction : " + ETAT
				+ ", "+ "chambres :" + chambres
				+ ", "+ "cuisines :" + cuisines
				+ ", "+ "sallesDeBain :" + sallesDeBain
				+ "]";
	}


	
}
