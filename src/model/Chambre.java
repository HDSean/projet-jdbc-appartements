package model;

/**
 * @file Chambre.java
 * @author Anica Sean & Ung Alexandre
 * @version 1.0
 * @date 17 Decembre 2020
 * @brief Définit la classe qui représente un objet Chambre et les fonctions basiques pour le manipuler.
 * @details cette classe hérite de local
 */
public class Chambre extends Local{

	private int idChambre;
	private String typeChambre;

	/**
	 * @brief constructeur de la classe chambre
	 * @param idChambre : Integer
	 * @param typeChambbre : String
	 * @param idLocal : Integer
	 * @param idAppartement : Integer
	 * @param surface : double
	 * @param description : String
	 */
	public Chambre(int idChambre, String typeChambbre, int idLocal, int idAppartement, double surface, String description) {
		super(idLocal, idAppartement, surface, description);
		this.setIdChambre(idChambre);
		this.setTypeChambre(typeChambbre);
	}

	/**
	 * @brief getter de l'attribut idChambre
	 * @return idChambre : Integer
	 */
	public int getIdChambre() {
		return idChambre;
	}

	/**
	 * @brief setter de l'attribut idChambre
	 * @param idChambre : Integer
	 */
	public void setIdChambre(int idChambre) {
		this.idChambre = idChambre;
	}

	/**
	 * @brief getter de l'attribut typeChambre
	 * @return typeChambre : String
	 */
	public String getTypeChambre() {
		return typeChambre;
	}

	/**
	 * @brief setter de l'attribut typeChambre
	 * @param typeChambre : String
	 */
	public void setTypeChambre(String typeChambre) {
		this.typeChambre = typeChambre;
	}
	
	/**
	 * @brief surcharge de la fonction toString pour afficher les attributs de la classe chambre
	 */
	@Override
	public String toString() {
		return String.format("\n\t\tLocal[idChambre : %d, typeChambre : %s, idLocal : %d, idAppartement : %s, Surface : %f m2, Description : %s]",
				this.getIdChambre(), this.getTypeChambre(), this.getIdLocal(), this.getIdAppartement(), this.getSurface(), this.getDescription());
	}
}
