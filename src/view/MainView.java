package view;

import java.util.ArrayList;
import java.util.Scanner;

import controller.AppartementDAO;
import controller.DAOFactory;
import controller.LiaisonDAO;
import model.Appartement;
import model.Chambre;
import model.Cuisine;
import model.Liaison;
import model.SalleDeBain;

/**
 * @class MainView.java
 * @version 7.0
 * @brief représente la classe de la vue qui est appelée dans le main, ici on affiche tout ce qui est demandé dans le sujet.
 * @details on utilise l'application en lignes de commandes mais il serait préférable de créer une interface utilisateur dans le future.
 * @author Sean Anica & Alexandre Ung
 * @date 17 Decembre 2020
 *
 */
public class MainView {
	private AppartementDAO appartementDAO;
	private LiaisonDAO liaisonDAO;
	public MainView() {
		setAppartementDAO(DAOFactory.getAppartementDAO());
		setLiaisonDAO(DAOFactory.getLiaisonDAO());
	}
	public void setLiaisonDAO(LiaisonDAO liaisonDAO) {
		this.liaisonDAO = liaisonDAO;
	}
	
	public void setAppartementDAO(AppartementDAO appartementDAO) {
		this.appartementDAO = appartementDAO;
	}
	
	/**
	 * @brief methode principale de la vue qui utilise toutes les fonctions plus bas.
	 * @details il s'agit d'un menu avec toutes les fonnctionnalités demandées dans le sujet.
	 */
	public void start() {
		System.out.println("\nVVVV=======================================================================================================================================VVVV");
		System.out.println("\t\tJDBC TP NOTE : SUJET SUR LA GESTION D'APPARTEMENTS\n");
		System.out.println("Voici la liste des appartements :\n");
		
		System.out.println("\n \t\tSelectionnez un choix parmi ceux affichés ci-dessous : ");
		System.out.println("\n \t\t1 - Concevoir un nouvel appartement\n");
		System.out.println("\n \t\t2 - Afficher la liste des appartements\n");
		System.out.println("\n \t\t3 - Recherche par caractéristiques spécifiques\n");
		System.out.println("\n \t\t4 - Lier des locaux entre eux\n");
		System.out.println("\n \t\tEntrez votre choix : \n-V-");
		Scanner sc = new Scanner(System.in);
		int choix = Integer.parseInt(sc.next());
		
		switch (choix) {
		case 1:
			ConcevoirNouvelAppartement();
			break;
		case 2:
			afficherAppartementsEtLocaux();
			break;
		case 3:
			rechercherParCaracteristiquesSpecifique();
			break;
		case 4:
			lierDesLocaux();
			break;
		default:
			System.out.println("Ce choix n'existe pas ! Veuillez choisir entre chiffre 1 et 4.\nL'application va redémarer dans 5 secondes.");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			start();
			break;
		}
		sc.close();
	}
	
	/**
	 * @brief methode qui permet de créer un appartement et de l'ajouter dans la base de données.
	 * @details fonctionne dans la vue en interaction avec un scanner.
	 */
	public void ConcevoirNouvelAppartement() {
		System.out.println("Entrez les informations de l'appartement que vous souhaitez créer->");
		String adresse,description; 
		int ec;
		boolean enconstruction = false;
		Scanner scanner = new Scanner(System.in);
		
		/*l'appartement*/
		System.out.println("Veuillez entrer l'adresse->");
		adresse = scanner.nextLine();
		System.out.println("Veuillez entrer la description de l'appartement->");
		description = scanner.nextLine();
		System.out.println("Veuillez entrer l'état de l'appartement, choix possibles[0 : en construction, 1 : prêt à livrer]->");
		ec = Integer.parseInt(scanner.next());
		if(ec == 1) {
			enconstruction = false;
		}else if(ec == 0){
			enconstruction = true;
		}else {
			System.out.println("Vous devez choisir entre 0 et 1!");
			start();
		}
		
		/*les cuisine*/
		int nbCuisines;
		ArrayList<Cuisine> cuisineList = new ArrayList<>();
		System.out.println("Entrez le nombre de Cuisines que vous souhaitez ajouter à l'appartement->");
		nbCuisines=Integer.parseInt(scanner.next());
		int numeroCuisine = 0;
		int nbPointsGaz = 0 ;
		String descriptionCuisine = "";
		double surfaceCuisine = 0.000;
		while(nbCuisines != 0) {
			numeroCuisine++;
			System.out.println("Entrez le nombre de points de gaz de la cuisine numéro [Entrez le même nombre de points de gaz pour toutes les cuisines]" + numeroCuisine +"->");
			nbPointsGaz = Integer.parseInt(scanner.next());
			System.out.println("Entrez la surface de la cuisine numéro " + numeroCuisine + "->");
			surfaceCuisine = Double.parseDouble(scanner.next());
			System.out.println("Entrez la description de la cuisine numéro " + numeroCuisine + "->");
			scanner.nextLine();
			descriptionCuisine = scanner.nextLine();
			cuisineList.add(new Cuisine(0, nbPointsGaz, 0, 0, surfaceCuisine, descriptionCuisine));
			nbCuisines--;
		}
		
		/*les salles de bain*/
		int nbSallesDeBain = 0;
		ArrayList<SalleDeBain> sallesdebainList = new ArrayList<>();
		System.out.println("Entrez le nombre de salles de bain que vous souhaitez ajouter à l'appartement->");
		nbSallesDeBain=Integer.parseInt(scanner.next());
		int numeroSalleDeBain = 0;
		int nbPointsEau = 0;
		String descriptionSalleDeBain = "";
		double surfaceSalleDeBain = 0.000;
		while(nbSallesDeBain != 0) {
			numeroSalleDeBain++;
			System.out.println("Entrez le nombre de points d'eau de la salle de bain numéro [Entrez le même nombre de points d'eau pour toutes les salles de bain] " + numeroSalleDeBain +"->");
			nbPointsEau = Integer.parseInt(scanner.next());
			System.out.println("Entrez la surface de la salle de bain numéro " + numeroSalleDeBain + "->");
			surfaceSalleDeBain = Double.parseDouble(scanner.next());
			System.out.println("Entrez la description de la salle de bain numéro " + numeroSalleDeBain + "->");
			scanner.nextLine();
			descriptionSalleDeBain = scanner.nextLine();
			sallesdebainList.add(new SalleDeBain(0, nbPointsEau, 0, 0, surfaceSalleDeBain, descriptionSalleDeBain));
			nbSallesDeBain--;
		}
		
		/*les chambres*/
		int nbChambres = 0;
		ArrayList<Chambre> chambresList = new ArrayList<>();
		System.out.println("Entrez le nombre de chambres que vous souhaitez ajouter à l'appartement->");
		nbChambres=Integer.parseInt(scanner.next());
		int numeroChambre = 0;
		String typeChambre = "";
		String descriptionChambre = "";
		double surfaceChambre = 0.000;
		while(nbChambres != 0) {
			numeroChambre++;
			System.out.println("Entrez le type de chambre de la chambre numéro " + numeroChambre +"->");
			typeChambre = scanner.next();
			System.out.println("Entrez la surface de la chambre numéro " + numeroChambre + "->");
			surfaceChambre = Double.parseDouble(scanner.next());
			System.out.println("Entrez la description de la chambre numéro " + numeroChambre + "->");
			scanner.nextLine();
			descriptionChambre = scanner.nextLine();
			chambresList.add(new Chambre(0, typeChambre, 0, 0, surfaceChambre, descriptionChambre));
			nbChambres--;
		}
				
		/*à la fin on crée l'appartement qu'on veut insérer*/
		Appartement appartement = new Appartement(0,adresse, description, enconstruction);
		appartement.setCuisines(cuisineList);//rajout des cuisines dans l'appartement
		appartement.setChambres(chambresList);//rajout des chambres dans l'appartement
		appartement.setSallesDeBain(sallesdebainList);//rajout des salles de bain dans l'appartement
		/*on crée les liaisons que l'on veut dans l'appartement*/
		ArrayList<Liaison> liaisonList = new ArrayList<>();
		System.out.println("Veuillez taper entrer pour passer à l'étape suivante->");
		scanner.nextLine();
		System.out.println("Voulez-vous lier des locaux entre eux(tapez 0 si vous ne connaissez pas les idlocal)?[1 : oui, 0 : non]->");
		int veutLier = Integer.parseInt(scanner.next());
		int idLocal1 = 0;
		int idLocal2 = 0;
		while(veutLier == 1) {
			System.out.println("Pour que vous puissiez lier des locaux entre eux, vous devez rentrer les idLocal des 2 locaux à lier-> \n");
			System.out.println("saisissez l'id du local 1 à lier->");
			idLocal1 = Integer.parseInt(scanner.next());
			System.out.println("saisissez l'id du local 2 à lier->");
			idLocal2 = Integer.parseInt(scanner.next());
			liaisonList.add(new Liaison(0,appartement.getIdAppartement(),idLocal1,idLocal2));
			System.out.println("Voulez-vous lier des locaux entre eux?[1 : oui, 0 : non]->");
			veutLier = Integer.parseInt(scanner.next());
		}
		
		appartement.setLiaisons(liaisonList);//rajout des liaisons dans l'appartement
		
		/*on insère l'appartement dans la bdd*/
		appartementDAO.insert(appartement);
		System.out.println("Votre appartement a bien été créé, regardez dans la liste des appartements ainsi que leur locaux : \n");
		afficherAppartementsEtLocaux();
		
		/*fermer le scanner*/
		scanner.close();
	}
	
	
	public void afficherAppartementsEtLocaux() {
		ArrayList<Appartement> aptList = appartementDAO.getAllAppartement();
		for(Appartement a: aptList) {
			System.out.println("\t" + a);
		}
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
	}
	
	/**
	 * @brief permet de rechercher des appartements par un critère spécifique
	 * @details critères possibles : par cuisine, salle de bain, chambre, surface de l'appartement.
	 */
	public void rechercherParCaracteristiquesSpecifique() {
		System.out.println("Selon quel critère souhaitez-vous rechercher des appartements?");
		System.out.println("1-Rechercher par le critère : cuisine");
		System.out.println("2-Rechercher par le critère : salle de bain");
		System.out.println("3-Rechercher par le critère : chambre");
		System.out.println("4-Rechercher par le critère : surface de l'appartement");
		Scanner scanner2 = new Scanner(System.in);
		int critere = Integer.parseInt(scanner2.next());
		
		switch (critere) {
		case 1:
			System.out.println("Vous êtes dans la recherche selon le critère : cuisine");
			System.out.println("Entrez le nombre de cuisines au minimum que vous voulez dans l'appartement->");
			int nbCuisine = Integer.parseInt(scanner2.next());
			ArrayList<Appartement> appartementList = appartementDAO.findAppartementsByNbCuisines(nbCuisine);
			if(appartementList.isEmpty()) {
				System.out.println("il n'y a pas d'appartement avec minimum " + nbCuisine + " cuisines");
				break;
			}
			System.out.println("Entrez le nombre de points de gaz au minimum vous voulez dans l'appartement->");
			int nbPointsGaz = Integer.parseInt(scanner2.next());
			ArrayList<Appartement> appartementListB = appartementDAO.findAppartementByPointsGaz(nbPointsGaz);
			ArrayList<Appartement> appartementListfinal = new ArrayList<Appartement>();
			for(Appartement b : appartementListB) {
				for(Appartement a : appartementList) {
					if(a.getIdAppartement() == b.getIdAppartement()) {
						appartementListfinal.add(a);
					}
				}
			}
			for(Appartement c: appartementListfinal) {
				System.out.println("\t" + c);
			}
			break;
		case 2:
			System.out.println("Vous êtes dans la recherche selon le critère : salle de bain");
			System.out.println("Entrez le nombre de salles de bain au minimum que vous voulez dans l'appartement->");
			int nbSallesDeBain = Integer.parseInt(scanner2.next());
			ArrayList<Appartement> appartementList2 = appartementDAO.findAppartementsByNbSallesDeBain(nbSallesDeBain);
			if(appartementList2.isEmpty()) {
				System.out.println("il n'y a pas d'appartement avec minimum " + nbSallesDeBain + " salles de bain");
				break;
			}
			System.out.println("Entrez le nombre de points d'eau au minimum vous voulez  dans l'appartement->");
			int nbPointsEau = Integer.parseInt(scanner2.next());
			ArrayList<Appartement> appartementListC = appartementDAO.findAppartementByPointsEau(nbPointsEau);
			ArrayList<Appartement> appartementListfinal2 = new ArrayList<Appartement>();
			for(Appartement b : appartementListC) {
				for(Appartement a : appartementList2) {
					if(a.getIdAppartement() == b.getIdAppartement()) {
						appartementListfinal2.add(a);
					}
				}
			}
			for(Appartement c: appartementListfinal2) {
				System.out.println("\t" + c);
			}
		
			break;
		case 3:
			System.out.println("Vous êtes dans la recherche selon le critère : chambre");
			System.out.println("Entrez le nombre de chambres au minimum que vous voulez dans l'appartement->");
			int nbChambres = Integer.parseInt(scanner2.next());
			ArrayList<Appartement> appartementList3 = appartementDAO.findAppartementsByNbChambres(nbChambres);
			if(appartementList3.isEmpty()) {
				System.out.println("il n'y a pas d'appartement avec minimum " + nbChambres + " chambres");
				break;
			}
			System.out.println("Entrez le type de chambre que vous voulez [1 : simple, 2 : double]->");
			int typeChambre = Integer.parseInt(scanner2.next());
			String TYPE = "";
			if(typeChambre == 1) {
				TYPE = "simple";
			}else if(typeChambre == 2) {
				TYPE = "double";
			}else {
				System.out.println("Vous ne pouvez choisir que les valeurs 1 ou 2 ! ");
				break;
			}
			ArrayList<Appartement> appartementListD = appartementDAO.findAppartementByTypeChambre(TYPE);
			ArrayList<Appartement> appartementListfinal3 = new ArrayList<Appartement>();
			for(Appartement b : appartementListD) {
				for(Appartement a : appartementList3) {
					if(a.getIdAppartement() == b.getIdAppartement()) {
						appartementListfinal3.add(a);
					}
				}
			}
			for(Appartement c: appartementListfinal3) {
				System.out.println("\t" + c);
			}
			
			break;
		case 4:
			System.out.println("Vous êtes dans la section de recherche par surface");
			System.out.println("Veuillez entrez la surface minimale de l'appartement souhaité->");
			double surface = Double.parseDouble(scanner2.next());
			ArrayList<Appartement> appartementListBySurface = appartementDAO.findAppartementsBySurface(surface);
			for(Appartement c: appartementListBySurface) {
				System.out.println("\t" + c);
			}
			break;
		default:
			System.out.println("Ce choix n'existe pas ! Veuillez choisir entre chiffre 1 et 4.\nL'application va redémarer dans 5 secondes.");
			try {
				Thread.sleep(5000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			start();
			break;
		}
		System.out.println("Entrez 0 pour revenir aux choix des critères ou 1 pour revenir au menu principal");
		int revenir = Integer.parseInt(scanner2.next());
		if(revenir == 0) {
			rechercherParCaracteristiquesSpecifique();
		}else {
			start();
		}
		scanner2.close();
		
	}
	
	/**
	 * @brief méthode permettant de lier des locaux entre eux.
	 * @details est utilisé dans la vue en interaction avec un scanner.
	 * 			On demande à l'utilisateur de saisir l'id de l'appartement ou se trouvent les locaux qu'il veut lier
	 * 			Puis les 2 locaux qu'il veut lier.
	 */
	public void lierDesLocaux() {
		@SuppressWarnings("resource")
		Scanner scanner = new Scanner(System.in);
		ArrayList<Liaison> liaisonList = new ArrayList<>();
		System.out.println("Veuillez taper entrer pour passer à l'étape suivante->");
		scanner.nextLine();
		System.out.println("si vous voulez lier des locaux vous devez connaitre la composition des appartements, nous vous l'affichons ici: \n");
		afficherAppartementsEtLocaux();
		System.out.println("Voulez-vous lier des locaux entre eux?[1 : oui, 0 : non]->");
		int veutLier = Integer.parseInt(scanner.next());
		int idLocal1 = 0;
		int idLocal2 = 0;
		int idAppartement = 0;
		while(veutLier == 1) {
			System.out.println("Pour que vous puissiez lier des locaux entre eux, vous devez rentrer les idLocal des 2 locaux à lier-> \n");
			System.out.println("saisissez l'id de l'appartement dans lequel vous voulez lier->");
			idAppartement = Integer.parseInt(scanner.next());
			System.out.println("saisissez l'id du local 1 à lier->");
			idLocal1 = Integer.parseInt(scanner.next());
			System.out.println("saisissez l'id du local 2 à lier->");
			idLocal2 = Integer.parseInt(scanner.next());
			liaisonList.add(new Liaison(0,idAppartement,idLocal1,idLocal2));
			System.out.println("Voulez-vous lier des locaux entre eux?[1 : oui, 0 : non]->");
			veutLier = Integer.parseInt(scanner.next());
		}
		for(Liaison l : liaisonList) {
		
			@SuppressWarnings("unused")
			boolean insert = liaisonDAO.insert(l);
		}
		System.out.println("Toutes les liaisons ont été effectuées avec succès.");
		System.out.println("pour revenir au menu, tapez 0 puis validez->");
		int back = Integer.parseInt(scanner.next());
		if(back == 0) {
			start();
		}
	}
}
